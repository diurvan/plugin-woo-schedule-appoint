# Plugin Programar Reserva Asientos

Plugin para reservar asientos, seleccionando fechas y horas permitidas. Existen Tipos de pasajeros (Niño, Adulto, Mayor de 60) para finalmente imprimir Tickets en formato QR.

![alt text](https://diurvanconsultores.com/wp-content/uploads/2020/05/woo_schedule_appoint_configbus.jpg?raw=true)
![alt text](https://diurvanconsultores.com/wp-content/uploads/2020/05/woo_schedule_appoint_configproducto.jpg?raw=true)
![alt text](https://diurvanconsultores.com/wp-content/uploads/2020/05/woo_schedule_appoint_reserva.jpg?raw=true)
![alt text](https://diurvanconsultores.com/wp-content/uploads/2020/05/woo_schedule_appoint_tickets.jpg?raw=true)

Repositorio Privado: https://gitlab.com/diurvan/woo-schedule-appoint